package calculator;

import calculator.actions.Addition;
import calculator.actions.Division;
import calculator.actions.Multiplication;
import calculator.actions.Subtraction;
import calculator.date.Date;


public class Main {
    public static void main(String[] args) {

    }

    public static double checkAddition(Date date){
        Addition addition = new Addition();
        addition.addition(date);
        return addition.addition(date);
    }

    public static double checkSubtraction(Date date){
        Subtraction subtraction = new Subtraction();
        subtraction.subtraction(date);
        return subtraction.subtraction(date);
    }

    public static double checkDivision(Date date){
        Division division = new Division();
        division.division(date);
        return division.division(date);
    }

    public static double checkMultiplication(Date date){
        Multiplication multiplication = new Multiplication();
        multiplication.multiplication(date);
        return multiplication.multiplication(date);
    }


}

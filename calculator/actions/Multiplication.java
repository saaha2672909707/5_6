package calculator.actions;

import calculator.date.Date;

public class Multiplication {

    public double multiplication(Date date){
        double result = date.getA() * date.getB();
        if(date.getAction().equals("*")){
            System.out.println("Result: " + result);
        }
        return result;
    }
}

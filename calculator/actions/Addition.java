package calculator.actions;

import calculator.date.Date;

public class Addition {

    public double addition(Date date){
        double resultAddition = date.getA() + date.getB();
        if(date.getAction().equals("+")){
            System.out.println("Result:" + resultAddition);
        }
        return resultAddition;
    }
}

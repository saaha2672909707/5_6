package calculator.actions;

import calculator.date.Date;

public class Subtraction {

    public double subtraction(Date date){
        double resultSubtraction = date.getA() - date.getB();
        if (date.getAction().equals("-")){
            System.out.println("Result: " + resultSubtraction);
        }
        return resultSubtraction;
    }
}

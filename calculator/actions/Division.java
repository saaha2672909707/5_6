package calculator.actions;

import calculator.date.Date;

public class Division {
    public  double division(Date date){
        double resultDivision = date.getA() / date.getB();
        if (date.getAction().equals("/")){
            System.out.println("Result:" + resultDivision);
        }
        return resultDivision;
    }
}

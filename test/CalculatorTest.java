package actiontsTest;

import calculator.Main;

import calculator.date.Date;
import junit.framework.Assert;
import org.junit.Test;

public class CalculatorTest {

    @Test
    public void additionTest(){
        double result = Main.checkAddition(new Date(1,"+",9));
        Assert.assertEquals(10,result,0.0);
    }

    @Test
    public void subtractionTest(){
        double result =  Main.checkSubtraction(new Date(2,"-",7));
        Assert.assertEquals(-5, result,0.0);
    }

    @Test
    public void divisionTest(){
        double result =  Main.checkDivision(new Date(54,"/",6));
        Assert.assertEquals(9,result,0.0);
    }

    @Test
    public void multiplicationTest(){
        double result =  Main.checkMultiplication(new Date(5,"*",10));
        Assert.assertEquals(50,result,0.0);
    }
}
